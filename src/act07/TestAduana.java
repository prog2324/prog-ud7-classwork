package act07;

/**
 *
 * @author sergio
 */
public class TestAduana {

    public static void main(String[] args) {
        PersonaFrancesa pf = new PersonaFrancesa("Napoleon");
        PersonaInglesa pi = new PersonaInglesa("John");
        pi.setPassaport("12341234");
        PersonaRusa pr = new PersonaRusa("Серджио");
        PersonaOtra po = new PersonaOtra("Sergio");
        Aduana aduana = new Aduana();

        aduana.entrar(pf);

        aduana.entrar(pr);
        
        aduana.entrar(pi);

        aduana.entrar(po);

    }
}
