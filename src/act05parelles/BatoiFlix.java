package act05parelles;

/**
 *
 * @author sergio
 */
public class BatoiFlix {

    public static void main(String[] args) {
        Data fecha = new Data(19, 2, 2021);
        Documental dreamSongs = new Documental("Dream Songs", Formato.MPG, 2400, fecha, "Enrique Juncosa", "Movimiento Hippie");
        dreamSongs.setDescripcion("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        fecha = new Data(20, 6, 2020);
        Pelicula newsOfTheWorld = new Pelicula("News of the World", Formato.AVI, 7200, fecha, "Tom Hanks", "Carolina Betller");
        newsOfTheWorld.setDescripcion("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        fecha= new Data(04, 06, 2001);
        Serie elHacker = new Serie("El hacker", Formato.FLV, 3600, fecha, 1, 20);
        elHacker.setDescripcion("Lorem Ipsum Lorem Ipsum Lorem Ipsum");

        System.out.println("\nListado Producciones (Uso toString()) \n");
        System.out.println(newsOfTheWorld);
        System.out.println(dreamSongs);
        System.out.println(elHacker);

        System.out.println("\nDetalle Producciones (Uso mostrarDetalle())\n");
        newsOfTheWorld.mostrarDetalle();
        dreamSongs.mostrarDetalle();
        elHacker.mostrarDetalle();
    }
}
