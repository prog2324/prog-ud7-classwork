package act01;

/**
 *
 * @author sergio
 */
public class PersonaInglesa extends Persona {
    @Override
    public void saluda() {
        System.out.println("Hi, I'm an english person");
    }
}
