package act01;

/**
 *
 * @author sergio
 */
public class PersonaFrancesa extends Persona {
    @Override
    public void saluda() {
        System.out.println("Bonjour, je suis français");
    }
}
