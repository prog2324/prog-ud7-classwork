package act09y10.enums;

public enum Size {
    BIG, MEDIUM, SMALL
};