package act09y10;

import act09y10.types.Animal;
//import act09.types.Lion;
//import act09.types.Tiger;

public class Vet {

    public void vaccinate(Animal animal) {
//        if (animal instanceof Lion
//                || animal instanceof Tiger) {
//            animal.eat();
//        }
        animal.vaccinate();
    }

    public void feed(Animal animal) {
        animal.eat();
    }

}
