package act03;

/**
 *
 * @author sergio
 */
public class Vehicle {
    private int velocitat;
    private int rodes;
    
    public Vehicle(int velocitat) {
        this.velocitat = velocitat;
        this.rodes = 4;
    }
    
    protected void accelerar() {
        this.velocitat += 10;
        System.out.println("Accelerant en VEHICLE");
    }
    
    public void frenar() {
        this.velocitat = 0;
    }
}
