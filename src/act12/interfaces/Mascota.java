package act12.interfaces;

/**
 *
 * @author sergio
 */
public interface Mascota {
    void serAmigable();
    void jugar();
}
