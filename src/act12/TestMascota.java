package act12;

import act12.enums.Size;
import act12.animaltypes.Cat;
import act12.animaltypes.Dog;
import act12.robottypes.RoboDog;

public class TestMascota {

    public static void main(String[] args) {

        Dog perro = new Dog(Size.SMALL, "Alcoy");
        Cat gato = new Cat(Size.SMALL, "Spain");
        RoboDog robot= new RoboDog(2000);
        
        Persona p = new Persona("Denís");
        
        p.interactuar(perro);
        p.interactuar(gato);
        p.interactuar(robot);          
        

    }

}
