package act12.enums;

public enum Size {
    BIG, MEDIUM, SMALL
};