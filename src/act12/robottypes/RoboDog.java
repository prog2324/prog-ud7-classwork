package act12.robottypes;

import act12.interfaces.Mascota;

/**
 *
 * @author sergio
 */
public class RoboDog extends Robot implements Mascota {
    
    public RoboDog(int creationYear) {
        super(creationYear);
    }

    @Override
    public void serAmigable() {
        System.out.println("Moviendo la cabeza");
    }

    @Override
    public void jugar() {
        System.out.println("Siguiendo una linea");
    }
    
}
