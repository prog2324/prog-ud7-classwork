package act12.robottypes;

/**
 *
 * @author sergio
 */
public abstract class Robot {
    int creationYear;
    
    public Robot(int creationYear) {
        this.creationYear = creationYear;
    }
    
    public void encender() {
        System.out.println("Encendiendo el Robot");
    }
}
