package act12.animaltypes;

import act12.enums.Food;
import act12.enums.Size;

public class Lion extends Animal {

    private static String NOMBRE = "León";

    public Lion(Size size, String location) {
        super(Food.CARNIVOROUS, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("ARGHHHHHHHH!!!!!!!!!");
    }

    @Override
    public void vaccinate() {
        super.vaccinate(); 
        super.eat();
    }
   
}
