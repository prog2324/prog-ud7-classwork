package act12.animaltypes;

import act12.enums.Food;
import act12.enums.Size;

public class Tiger extends Animal {

    private static final String NOMBRE = "Tigre";

    public Tiger(Size size, String location) {
        super(Food.CARNIVOROUS, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("OAUGGGGGGG....!");
    }

    @Override
    public void vaccinate() {
        super.vaccinate();
        super.eat();
    }
}
