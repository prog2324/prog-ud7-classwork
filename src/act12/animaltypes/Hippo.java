package act12.animaltypes;

import act12.enums.Food;
import act12.enums.Size;

public class Hippo extends Animal {

    private static String NOMBRE = "Hipopótamo";

    public Hippo(Size size, String location) {
        super(Food.HERBIVOROUS, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("HIPPPP!!!!!!!!!");
    }
}
