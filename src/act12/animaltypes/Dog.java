package act12.animaltypes;

import act12.enums.Food;
import act12.enums.Size;
import act12.interfaces.Mascota;

public class Dog extends Animal implements Mascota {

    private static String NOMBRE = "Perro";

    public Dog(Size size, String location) {
        super(Food.OMNIVORE, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("GUAU!!!");
    }

    @Override
    public void serAmigable() {
        System.out.println("Moviendo la cola");
    }

    @Override
    public void jugar() {
        System.out.println("Jugando con el frisbee");
    }
}
