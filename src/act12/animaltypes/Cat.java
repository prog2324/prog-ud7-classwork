package act12.animaltypes;

import act12.enums.Food;
import act12.enums.Size;
import act12.interfaces.Mascota;


public class Cat extends Animal implements Mascota {

    private static String NOMBRE = "Gato";
    
    public Cat(Size size, String location) {
        super(Food.CARNIVOROUS, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("Miauuuu!!!!!!!!!");
    }

    @Override
    public void serAmigable() {
        System.out.println("Ronroneando");
    }

    @Override
    public void jugar() {
        System.out.println("Jugando con el ovillo de lana");
    }
}
